/* Filename: route.js
 */

define(['marionette'], function (Maironette) {
	return Marionette.AppRouter.extend({

	    routes : {
	      "main": "showMain",
	      "test": "showTest"
	    },

	    showMain: function () {
	    	console.log("Show Main");
	    	require(['views/menubar'], function(MenubarView){
                var mv = new MenubarView({model: new Backbone.Model({name: "Doe"})});
                window.app.top.show(mv);
	    	});
	    },

	    showTest : function () {
	    	console.log("just a test");
	    }
	});
});
