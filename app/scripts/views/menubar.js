/*global define*/

define([
    'backbone',
    'marionette',
    'itemplates'
], function (Backbone, Marionette, templates) {

    var MenubarView = Marionette.ItemView.extend({
        template: templates.mb,

        tagName: 'div',

        ui:{
            name : "#name"
        },

        events:{
            'change @ui.name': 'updateName'
        },

        initialize: function () {
            this.listenTo(this.model, 'change', this.render);
        },

        onShow: function(){

        },

        updateName: function(){
            console.log(this.ui.name.val());
            this.model.set("name", this.ui.name.val());
        },

        onRender: function(){

            console.log(this.model);
        }
    });

    return MenubarView;
});
