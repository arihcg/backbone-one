define(['backbone',
        'marionette',
        'handlebars',
        'route'
    ], function(Backbone, Marionette, Handlebars, Router){

    // Replace underscore.js with handlebars.js template complier
	Marionette.TemplateCache.prototype.compileTemplate = function (rawTemplate) {
		return Handlebars.compile(rawTemplate);
	};

    var app = new Marionette.Application();

    app.addRegions({
        top: "#top",
        main: "#main"
    });

    var router = new Router();

	app.on('start', function() {
		Backbone.history.start();
	});

    window.app = app;
	return app;
})
